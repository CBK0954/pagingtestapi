package com.example.choibk.pagingtestapi.entity;

import com.example.choibk.pagingtestapi.Interfaces.CommonModelBuilder;
import com.example.choibk.pagingtestapi.model.BoardDocumentCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardDocument {
    @ApiModelProperty(notes = "시퀀스")
    @Id@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "게시글 제목")
    @Column(nullable = false, length = 100)
    private String title;

    @ApiModelProperty(notes = "게시글 내용")
    @Column(nullable = false, columnDefinition = "TEXT")
    private String contents;

    @ApiModelProperty(notes = "작성자명")
    @Column(nullable = false, length = 20)
    private String writerName;

    @ApiModelProperty(notes = "작성일")
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @ApiModelProperty(notes = "조회수")
    @Column(nullable = false)
    private Integer viewCount;

    @ApiModelProperty(notes = "댓글수")
    @Column(nullable = false)
    private Integer commentCount;

    private BoardDocument(BoardDocumentBuilder builder) {
        this.title = builder.title;
        this.contents = builder.contents;
        this.writerName = builder.writerName;
        this.dateCreate = builder.dateCreate;
        this.viewCount = builder.viewCount;
        this.commentCount = builder.commentCount;
    }

    public static class BoardDocumentBuilder implements CommonModelBuilder<BoardDocument> {

        private final String title;
        private final String contents;
        private final String writerName;
        private final LocalDateTime dateCreate;
        private final Integer viewCount;
        private final Integer commentCount;

        public BoardDocumentBuilder(BoardDocumentCreateRequest createRequest) {
            this.title = createRequest.getTitle();
            this.contents = createRequest.getContents();
            this.writerName = createRequest.getWriterName();
            this.dateCreate = LocalDateTime.now();
            this.viewCount = 0;
            this.commentCount = 0;
        }
        @Override
        public BoardDocument build() {
            return new BoardDocument(this);
        }
    }
}
