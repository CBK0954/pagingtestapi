package com.example.choibk.pagingtestapi.service;

import com.example.choibk.pagingtestapi.entity.BoardComment;
import com.example.choibk.pagingtestapi.model.BoardCommentCreateRequest;
import com.example.choibk.pagingtestapi.model.BoardCommentItem;
import com.example.choibk.pagingtestapi.model.ListResult;
import com.example.choibk.pagingtestapi.repository.BoardCommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardCommentService {
    private final BoardCommentRepository boardCommentRepository;
    public void setComment(long boardDocumentId, BoardCommentCreateRequest createRequest) {
        BoardComment addData = new BoardComment.BoardCommentBuilder(boardDocumentId, createRequest).build();

        boardCommentRepository.save(addData);
    }

    public ListResult<BoardCommentItem> getComments(long documentId, int pageNum) {
        Page<BoardComment> originList = boardCommentRepository.findAllByBoardDocumentIdOrderByIdDesc(documentId, ListConvertService.getPageable(pageNum,15));

        List<BoardCommentItem> result = new LinkedList<>();
        for(BoardComment item : originList.getContent()) {
            result.add(new BoardCommentItem.BoardCommentItemBuilder(item).build());
        }

        return ListConvertService.settingResult(
                result,
                originList.getTotalElements(),
                originList.getTotalPages(),
                originList.getPageable().getPageNumber()
        );
    }
}
