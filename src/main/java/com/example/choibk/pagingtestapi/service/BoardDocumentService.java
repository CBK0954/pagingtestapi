package com.example.choibk.pagingtestapi.service;

import com.example.choibk.pagingtestapi.entity.BoardDocument;
import com.example.choibk.pagingtestapi.model.BoardDocumentCreateRequest;
import com.example.choibk.pagingtestapi.model.BoardDocumentItem;
import com.example.choibk.pagingtestapi.model.ListResult;
import com.example.choibk.pagingtestapi.repository.BoardDocumentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiPredicate;

@Service
@RequiredArgsConstructor
public class BoardDocumentService {
    private final BoardDocumentRepository boardDocumentRepository;

    public void setDocument(BoardDocumentCreateRequest createRequest) {
        BoardDocument addData = new BoardDocument.BoardDocumentBuilder(createRequest).build();

        boardDocumentRepository.save(addData);
    }
    public ListResult<BoardDocumentItem> getDocuments(int pageNum) {
        // 원본 리스트를 가져옴, 페이징을 하기위해 List<> -> Page<> 형태로 가져옴
        // 페이징을 해야하기 때문에 몇번 페이지를 보려고 하는지, 페이지당 몇개의 item 을 보여줄건지 알려줘야한다.
        // 따라서 ListConvertService.getPageable(원하는 페이지 번호)을 이용해 PageRequest(Pageable 을 구현한 함수가 PageRequest 라서 결국 같은 값)을 넘겨준다.
        Page<BoardDocument> originList = boardDocumentRepository.findAll(ListConvertService.getPageable(pageNum));

        List<BoardDocumentItem> result = new LinkedList<>(); // 결과 리스트를 담을 빈 리스트 생성
        for(BoardDocument item : originList.getContent()) { // 원본 리스트에서 원본을 차례대로 하나씩 던져준다.
                                                            // originList 는 Page 객체라서 List 를 빼내려면 getContents 라고 정확히 명시해서 리스트만 빼와야한다
            result.add(new BoardDocumentItem.BoardDocumentBuilder(item).build()); // 재가공 후 result 에 넣어준다.
        }
        // ListConvertService.settingResult 를 이용해서 List 를 ListResult 로 바꿔준다.
        // 이번 경우는 1페이지 중 1페이지가 아니기 때문에
        // 원본의 총 개수(원본 개수만큼 재가공해서 보여주는 것이기 때문에 원본 개수와 재가공 개수가 같다)
        // 총 몇 페이지 인지, 현재 몇 페이지 인지, 다 알려준뒤 List 를 ListResult 로 바꿔달라고 요청한다.
        return ListConvertService.settingResult(
                result,
                originList.getTotalElements(),
                originList.getTotalPages(),
                originList.getPageable().getPageNumber()
        );
    }
}
