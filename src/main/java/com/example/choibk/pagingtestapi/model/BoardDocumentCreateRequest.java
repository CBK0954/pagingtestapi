package com.example.choibk.pagingtestapi.model;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BoardDocumentCreateRequest {
    @ApiModelProperty(notes = "게시글 제목 (2 ~ 100자)", required = true)
    @NotNull
    @Length(min = 2, max = 100)
    private String title;

    @ApiModelProperty(notes = "게시글 내용 (10자 이상)", required = true)
    @NotNull
    @Length(min = 10)
    private String contents;

    @ApiModelProperty(notes = "작성자명 (2 ~ 20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String writerName;
}
