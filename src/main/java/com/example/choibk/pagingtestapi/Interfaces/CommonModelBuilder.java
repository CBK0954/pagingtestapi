package com.example.choibk.pagingtestapi.Interfaces;

public interface CommonModelBuilder<T> {
    T build();
}