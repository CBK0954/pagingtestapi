package com.example.choibk.pagingtestapi.repository;

import com.example.choibk.pagingtestapi.entity.BoardDocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardDocumentRepository extends JpaRepository<BoardDocument, Long> {
}
