package com.example.choibk.pagingtestapi.controller;

import com.example.choibk.pagingtestapi.model.BoardCommentCreateRequest;
import com.example.choibk.pagingtestapi.model.BoardCommentItem;
import com.example.choibk.pagingtestapi.model.CommonResult;
import com.example.choibk.pagingtestapi.model.ListResult;
import com.example.choibk.pagingtestapi.service.BoardCommentService;
import com.example.choibk.pagingtestapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "댓글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board-comment")
public class BoardCommentController {
    private final BoardCommentService boardCommentService;

    @ApiOperation(value = "댓글 등록")
    @PostMapping("/document-id/{documentId}")
    public CommonResult setComment(@PathVariable long documentId, @RequestBody @Valid BoardCommentCreateRequest createRequest) {
        boardCommentService.setComment(documentId, createRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "댓글 리스트")
    @GetMapping("/all/document-id/{documentId}/page/{pageNum}")
    public ListResult<BoardCommentItem> getComments(@PathVariable long documentId, @PathVariable int pageNum) {
        return ResponseService.getListResult(boardCommentService.getComments(documentId, pageNum), true);
    }
}
